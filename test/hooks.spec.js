describe('Mocha Hooks', function() {
    before('Execute before all tests', function() {
        console.log('Execute before all tests');
    })

    beforeEach('Execute before each tests', function() {
        console.log('Execute before each tests');
    })

    after('Execute after all tests', function() {
        console.log('Execute after all tests');
    })

    afterEach('Execute after each tests', function() {
        console.log('Execute after each tests');
    })

    it('This Hooks Test', function() {
        console.log('This is my test');
    })

    it('This is a test for pending test feature');
}) 