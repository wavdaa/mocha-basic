# Installation
1. Install nodejs
2. Install npm
3. run `npm install mocha`

# Execution
- Option 1: `npm test`
- Option 2: `mocha test/ --reporter json` (test report options: json, dot, nyan)
